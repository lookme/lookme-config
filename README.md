# lookme-config

## 紹介

複数の設定ファイルを解析して設定を管理するライブラリです。

## 機能と特徴

* 複数の設定ファイル(YAML)
* ディープマージ
* 設定取り出す時チェック可能

## 使い方

### Quick Start

設定ファイルconfig/foo.yml

    session:
      expire_after: 3600
      redis_key_prefix: 'sk:sessions:'
      redis_url: redis://127.0.0.1:6379/2
      domain:
      
テスト

    irb > require 'lookme-config'
     => true
    irb > conf = LookmeConfig::Configuration.from_files(['config/foo.yml'])
    I, [2017-09-06T10:53:27.804868 #16018]  INFO -- LookmeConfig::Configuration: load configurations from files ["config/foo.yml"]
    I, [2017-09-06T10:53:27.806965 #16018]  INFO -- LookmeConfig::Configuration: load configuration from config/foo.yml
    irb > conf['session']['redis_url'].get
     => "redis://127.0.0.1:6379/2"
    irb > conf['session']['domain'].get
     => nil
    irb > conf['session']['domain'].not_blank
    KeyError: option not found, key session.domain
            from /Users/xxx/.rvm/gems/ruby-2.1.5/bundler/gems/lookme-config-bf64166f24ea/lib/lookme_config/cascade_option.rb:54:in `ensure_value_present!'
            from /Users/xxx/.rvm/gems/ruby-2.1.5/bundler/gems/lookme-config-bf64166f24ea/lib/lookme_config/cascade_option.rb:26:in `not_blank'
            from (irb):6
            from /Users/xxx/.rvm/rubies/ruby-2.1.5/bin/irb:11:in `<main>'

まとめてみると、

1. LookmeConfig::Configuration#from_filesで設定ファイルを読み込み
2. conf['foo']['bar'].get で設定値を取り出す

### 設定ファイルの読み込み

* 複数ファイル可
* 設定値の取り出す順番は左から右へ
* 存在してないファイルや空きのファイルパスはスルーする、エラーにならない
* すくなくとも1つ有効な設定ファイルのパスは必要

### Railsの環境は直接対応してない

設定ファイルにdevelopment, productionなどを入れて `config[Rails.env]` みたいな形で取り出すことができますが、lookme-configでは、環境別の設定ファイルは分けるのをおすすめします。

具体的には、

* application.yml
* application-development.yml
* application-production.yml

を用意して、初期化するとき

    APPLICATION_CONFIGURATION = LookmeConfig::Configuration.from_files(["application-#{Rails.env}.yml", 'application.yml'])

で別々環境の設定が読み込まれます。

### 環境ごとの設定ファイル

設定ファイルの中で環境などのキーを入れてないのは、そのキーはすべての環境を対応しきれない理由もあります。

例えば、負荷テストのサーバーでは、performance_testなどのキーを入れてなくて、違う設定をapplication-performace-test.ymlに追記すれば済みます。もっと使いやすいためには、こういう初期化する方法をおすすめします。

    application_configuration = LookmeConfig::Configuration.from_files([
      ENV['APPLICATION_CONFIGURATION_PATH'],
      'application.yml'
    ])

環境変数でその「環境」に該当する設定ファイルを入力します。

### 設定値のチェック

アプリにとっては、必須とそうではない設定値があります。それと、一部設定値は数値ではないと変なエラーになるケースもあります。lookme-configでは

* 設定値があるかないか
* 設定値のタイプ（数値、文字列）

があります。APIでは

* get、そのまま値を取り出す、ない時はnilになる
* required、ない時エラーになる
* not_blank、ない時エラーになる、文字列タイプチェック
* as_int、ない時エラーになる、数値タイプチェック

幾つかのメソッドがります。