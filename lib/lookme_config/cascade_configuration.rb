module LookmeConfig
  class CascadeConfiguration
    def initialize(configs)
      @configs = configs
    end

    def [](key)
      CascadeOption.new(@configs, [key])
    end
  end
end