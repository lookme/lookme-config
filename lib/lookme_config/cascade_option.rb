module LookmeConfig
  class CascadeOption
    # @param [Array<Sougoukanri::Config::Configuration>] configs
    # @param [Array<String>] keys
    def initialize(configs, keys)
      @configs = configs
      @keys = keys
    end

    # @param [String] key
    def [](key)
      CascadeOption.new(@configs, @keys + [key])
    end

    # @return [Object]
    def required
      ensure_value_present!
    end

    def get
      find_value(@configs, @keys)
    end

    # @return [String]
    def not_blank
      value = ensure_value_present!
      unless value.is_a?(String)
        raise ::TypeError, "option #{full_key} is not a string, value #{value.inspect}"
      end
      raise "option #{full_key} is blank" if value == ''
      value
    end

    def as_int
      value = ensure_value_present!
      case
        when String
          Integer(value)
        when Integer
          value
        else
          raise ::TypeError, "unexpected option #{full_key} type #{value.inspect}"
      end
    end

    private

    def full_key
      @keys.join('.')
    end

    def ensure_value_present!
      value = find_value(@configs, @keys)
      raise ::KeyError, "option not found, key #{full_key}" if value.nil?
      value
    end

    # @param [Array<LookmeConfig::Configuration>] configs
    # @param [Array<String>] keys
    def find_value(configs, keys)
      return nil if configs.empty? || keys.empty?
      value = nil
      configs.each do |c|
        value = c.get(keys)
        break unless value.nil?
      end
      value
    end
  end
end