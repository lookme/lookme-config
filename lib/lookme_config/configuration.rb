module LookmeConfig
  class Configuration
    LOG_TAG = self.name

    class << self
      # @param [Array<String>] files
      def from_files(files)
        logger = LookmeLogging::LoggerManager.find_logger('LookmeConfig::Configuration')
        logger.info(LOG_TAG) {"load configurations from files #{files}"}
        valid_files = files.select {|f| f && ::File.exist?(f)}
        raise ::ArgumentError, 'at least 1 file required' if valid_files.empty?

        configs = valid_files.map {|f|
          logger.info(LOG_TAG) {"load configuration from #{f}"}
          create_configuration(f)
        }
        CascadeConfiguration.new(configs)
      end

      private

      # @param [String] yaml_file_path
      def create_configuration(yaml_file_path)
        content = ::ERB.new(::File.read(yaml_file_path)).result
        HashConfiguration.new(::YAML.load(content))
      end
    end
  end
end