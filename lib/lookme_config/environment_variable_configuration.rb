module LookmeConfig
  # foo.bar => FOO_BAR
  class EnvironmentVariableConfiguration
    # @param [Array<String>] keys
    def get(keys)
      return nil if keys.empty?
      key = keys.map {|k| k.upcase}.join('_')
      ENV[key]
    end
  end
end