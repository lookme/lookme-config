module LookmeConfig
  class HashConfiguration
    # @param [Hash] options
    def initialize(options)
      @options = options
    end

    # @param [Array<String>] keys
    # @return [Object]
    def get(keys)
      return nil if keys.empty?
      current = @options
      key_index = 0
      while key_index < keys.length
        if current
          current = current[keys[key_index]]
        end
        key_index += 1
      end
      current
    end
  end
end